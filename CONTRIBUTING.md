# Contributing to this repository

1. Made a change to a package (add a feature, fix a bug, enhance doc etc.)
2. Commit your changes following conventional commits with the package name as scope

example: 
```shell 
$ git commit -m "feat(package-name): my awesome feature 💪"
```

3. publish with lerna using the deploy script
   
```shell
$ yarn run deploy
```