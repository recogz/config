# Recog NPM config packages

> 🚀 The Recog NPM config packages in a Lerna managed monorepository

- [**Prettier**](packages/prettier-config) config
- [**Semantic Release**](packages/semantic-release-config) config
- TODO: **ESLint** config

## Usage
### Registry setup
```shell
echo @recog:registry=https://gitlab.com/api/v4/projects/27696766/packages/npm/ >> .npmrc
```
### Authentication
Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the scope set to `api`.
```shell
echo //gitlab.com/api/v4/projects/27696766/packages/npm/:_authToken=<your_token> >> .npmrc
```
### Installation
```shell
yarn i @recog/<package_name>
```


## Contributing

Please check [CONTRIBUTING.md](CONTRIBUTING.md).
