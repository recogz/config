# `semantic-release-config`

> 🏷 Recog semantic release config
 
## Plugins

This shareable configuration use the following plugins:

- [`@semantic-release/commit-analyzer`](https://github.com/semantic-release/commit-analyzer)
- [`@semantic-release/release-notes-generator`](https://github.com/semantic-release/release-notes-generator)
- [`@semantic-release/changelog`](https://github.com/semantic-release/changelog)
- [`@semantic-release/npm`](https://github.com/semantic-release/npm)
- [`@semantic-release/git`](https://github.com/semantic-release/git)
- [`@semantic-release/gitlab`](https://github.com/semantic-release/gitlab)

## Usage
### Registry setup
```shell
echo @recog:registry=https://gitlab.com/api/v4/projects/27696766/packages/npm/ >> .npmrc
```
### Authentication
Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the scope set to `api`.
```shell
echo //gitlab.com/api/v4/projects/27696766/packages/npm/:_authToken=<your_token> >> .npmrc
```
### Installation
```shell
yarn i -D @recog/semantic-release-config
```
**NOTE:** if you are using _lerna_, add the `-W` option.

Then the shareable config can be configured in the [**semantic-release** configuration file](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#configuration):

```json
{
  "extends": "@recog/semantic-release-config"
}
```