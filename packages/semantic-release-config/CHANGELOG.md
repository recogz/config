# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.3](https://gitlab.com/recogz/config/compare/@recog/semantic-release-config@1.2.2...@recog/semantic-release-config@1.2.3) (2023-04-13)


### Bug Fixes

* **semantic-release-config:** add dev to branches ([132503e](https://gitlab.com/recogz/config/commit/132503ed1e5d50600d8e7f6324831a99e2aa3223))





## [1.2.2](https://gitlab.com/recogz/config/compare/@recog/semantic-release-config@1.2.1...@recog/semantic-release-config@1.2.2) (2023-01-23)


### Bug Fixes

* **semantic-release:** semantic-release version range + branches ([1ef4d32](https://gitlab.com/recogz/config/commit/1ef4d3208886b5c6e35e0b82225613e3e1bdbc6a))





## [1.2.1](https://gitlab.com/recogz/config/compare/@recog/semantic-release-config@1.2.0...@recog/semantic-release-config@1.2.1) (2023-01-23)


### Bug Fixes

* **semantic-release:** invalid main field in package.json ([9406df9](https://gitlab.com/recogz/config/commit/9406df9ccc0e7b0f428ad1ec0539d12b5c459995))





# [1.2.0](https://gitlab.com/recogz/config/compare/@recog/semantic-release-config@1.1.3...@recog/semantic-release-config@1.2.0) (2023-01-05)


### Features

* **semantic-release-config:** add prerelease branch beta ([fc86f9a](https://gitlab.com/recogz/config/commit/fc86f9a1a8b2a186698cad960efd6a66f56d0f0a))





## [1.1.3](https://gitlab.com/recogz/config/compare/@recog/semantic-release-config@1.1.2...@recog/semantic-release-config@1.1.3) (2021-10-28)

**Note:** Version bump only for package @recog/semantic-release-config





## [1.1.2](https://gitlab.com/recogz/config/compare/@recog/semantic-release-config@1.1.1...@recog/semantic-release-config@1.1.2) (2021-07-05)


### Bug Fixes

* add main as allowed branches ([6bd694e](https://gitlab.com/recogz/config/commit/6bd694ebce6d201077f25d33e18d1b65bb98b70b))





## [1.1.1](https://gitlab.com/recogz/config/compare/@recog/semantic-release-config@1.1.0...@recog/semantic-release-config@1.1.1) (2021-07-02)


### Bug Fixes

* **semantic-release-config:** remove semantic-release-monorepo ([7eecd7b](https://gitlab.com/recogz/config/commit/7eecd7b798fc2dab997a1ec57632ca59b8a5f01b))





# [1.1.0](https://gitlab.com/recogz/config/compare/@recog/semantic-release-config@1.0.0...@recog/semantic-release-config@1.1.0) (2021-07-02)


### Features

* **semantic-release-config:** add extends semantic-release-monorepo ([5745968](https://gitlab.com/recogz/config/commit/57459687cd9e4ec1b9503d58d3d8de7986250a03))





# 1.0.0 (2021-07-01)


### Features

* 📦 add new package semantic-release config ([4db9049](https://gitlab.com/recogz/config/commit/4db90494c9abf5915bec9c940313f932d179efbd))
