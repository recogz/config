# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.1](https://gitlab.com/recogz/config/compare/@recog/prettier-config@1.1.0...@recog/prettier-config@1.1.1) (2021-10-28)

**Note:** Version bump only for package @recog/prettier-config





# [1.1.0](https://gitlab.com/recogz/packages/compare/@recog/prettier-config@1.0.0...@recog/prettier-config@1.1.0) (2021-06-28)


### Features

* **prettier-config:** set printWidth to 120 + add some docs ([5332a23](https://gitlab.com/recogz/packages/commit/5332a23b26462e14baa19230d6994b7f6f89aa22))





# 1.0.0 (2021-06-28)

**Note:** Version bump only for package @recog/prettier-config
