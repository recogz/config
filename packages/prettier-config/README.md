# `prettier-config`

> 🚀 Recog prettier config

## Usage
### Registry setup
```shell
echo @recog:registry=https://gitlab.com/api/v4/projects/27696766/packages/npm/ >> .npmrc
```
### Authentication
Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the scope set to `api`.
```shell
echo //gitlab.com/api/v4/projects/27696766/packages/npm/:_authToken=<your_token> >> .npmrc
```
### Installation
```shell
yarn i -D @recog/prettier-config
```
**NOTE:** if you are using _lerna_, add the `-W` option.

Then, in your `package.json`:
```json
"prettier": "@recog/prettier-config"
```