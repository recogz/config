# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.5](https://gitlab.com/recogz/config/compare/@recog/eslint-config-react@1.1.4...@recog/eslint-config-react@1.1.5) (2022-01-26)


### Bug Fixes

* **eslint-config-react:** changing eslint-plugin-import version from latest to 2.21.2 ([e4675a6](https://gitlab.com/recogz/config/commit/e4675a6c2bfad71f9e5afd7de741aab5af1dc3cc))





## [1.1.4](https://gitlab.com/recogz/config/compare/@recog/eslint-config-react@1.1.3...@recog/eslint-config-react@1.1.4) (2022-01-25)


### Bug Fixes

* **eslint-config-react:** revert some rules ([d9bd548](https://gitlab.com/recogz/config/commit/d9bd548e68df0300ebce7bf644373ef64f942ec8))





## [1.1.3](https://gitlab.com/recogz/config/compare/@recog/eslint-config-react@1.1.2...@recog/eslint-config-react@1.1.3) (2022-01-25)


### Bug Fixes

* **eslint-config-react:** fixing fieldname ([ea02dbf](https://gitlab.com/recogz/config/commit/ea02dbf4d266b6be6fb6f5ce7369b66d5ef59865))





## 1.1.2 (2022-01-25)


### Bug Fixes

* **eslint-config-react:** changing package name arcoding to eslint doc ([2554afd](https://gitlab.com/recogz/config/commit/2554afd6507e988144df121f2ec3127b4d8b567b))





## [1.1.1](https://gitlab.com/recogz/config/compare/@recog/react-eslint-config@1.1.0...@recog/react-eslint-config@1.1.1) (2022-01-25)


### Bug Fixes

* **react-eslint-config:** switching json from js config ([abfac66](https://gitlab.com/recogz/config/commit/abfac660518aeafb57c6c4f9eb3455a321b6427f))





# 1.1.0 (2022-01-25)


### Features

* adding react-eslint-config ([38590fa](https://gitlab.com/recogz/config/commit/38590fa6681829fbdc3ce1e42a0caaed85b0b2b4))
